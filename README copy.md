<p>Please follow this instruction to install the database in your local computer</p>
<br>
<ul>
    <li>Clone this project to your repository</li>
    <li>Make sure the environment ready for Laravel 9.xx PHP 7.xx and Mysql Database/similar</li>
    <li>Open root folder and config .env file using local database configuration</li>
    <li>Open Terminal on root folder and make sure Laravel works properly by running "php artisan serve"</li>
    <li>Run "php artisan migrate" to install the database</li>
    <li>Run "php artisan db:seed" to create necessary dummy records.</li>
    <li>Laravel and database now ready.</li>
</ul>