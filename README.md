<p>Please follow this instruction to install the database in your local computer</p>
<br>
<ul>
    <li>Clone this project to your repository</li>
    <li>Make sure the environment ready for Laravel 9.xx PHP 7.xx and Mysql Database/similar</li>
    <li>Open root folder and config .env file using local database configuration</li>
    <li>Open Terminal on root folder and make sure Laravel works properly by running "php artisan serve"</li>
    <li>Run "php artisan migrate" to install the database</li>
    <li>Run "php artisan db:seed" to create necessary dummy records.</li>
    <li>Run "php artisan tinker" to check the relation on cart or order.</li>
    <li>Run "App\Models\Cart::where("id",1)->with('items.detail.product','items.detail.variants.variant','owner')->first();" to check cart</li>
    <li>Run "App\Models\Cart::where("id",2)->with('items.detail.product','items.detail.variants.variant','owner','shipping')->first();" to check order</li>
    <li>Laravel and database now ready.</li>
</ul>

<h3>API</h3>
Please run "php artisan db:seed --class=MakeRedTshirtSold" to make sure the sold items works on product detail.
<br><br>
Endpoint: 
<br>
<strong>GET</strong> [domain]/api/product/tshirt      -> check product tshirt variant & available<br>
<strong>GET</strong> [domain]/api/product/keripik     -> check product kerpipik variant & available

<br><br>

Response example<br>
<pre>
{
    "data": {
        "id": 1,
        "created_at": "2024-03-01T04:00:11.000000Z",
        "updated_at": "2024-03-01T04:00:11.000000Z",
        "deleted_at": null,
        "user_id": 1,
        "name": "T-Shirt",
        "description": null,
        "category_id": null,
        "sku": "tshirt",
        "variants": {
            "color": {
                "name": "Color",
                "options": [
                    {
                        "id": 1,
                        "name": "Red"
                    },
                    {
                        "id": 2,
                        "name": "Green"
                    },
                    {
                        "id": 3,
                        "name": "Blue"
                    }
                ]
            },
            "size": {
                "name": "Size",
                "options": [
                    {
                        "id": 4,
                        "name": "S"
                    },
                    {
                        "id": 5,
                        "name": "M"
                    },
                    {
                        "id": 6,
                        "name": "L"
                    }
                ]
            }
        },
        "available_variant_combinations": [
            [
                4,
                2
            ],
            [
                5,
                2
            ],
            [
                6,
                2
            ],
            [
                4,
                3
            ],
            [
                5,
                3
            ],
            [
                6,
                3
            ]
        ],
        "sold_out_variants": [
            1
        ]
    }
}
</pre>
<br>
<strong>NOTES:</strong> <br>
<pre>available_variant_combinations</pre>  => collections of available variant combination, for example: [4,2] it means variant 4 (for size S) and 2 (for color Green) is available<br>
<pre>sold_out_variants</pre> => ID collection of sold out variants.
