<?php

namespace App\Http\Controllers\apis;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\{Product};

class ProductController extends Controller
{
    public function detail(Request $request, $sku){
        $data   = Product::where("sku",$sku)->with("variants","available_stocks.variants.variant")->first();

        if($data){
            $variants = [];
            $data_all_variants = [];

            if($data['variants']){
                foreach($data['variants'] as $indexvariant => $variant){
                    $variants[$variant->slug]['name']          = ucwords(str_replace("_"," ",$variant->slug));
                    $variants[$variant->slug]['options'][]     = [
                        "id"                => $variant->id,
                        "name"              => $variant->name,
                    ];

                    $data_all_variants[] = $variant->id;
                }
            }

            $variant_combinations = [];
            if($data['available_stocks']){
                foreach($data['available_stocks'] as $indexstock => $stock){
                    if($stock['variants']){
                        $new_combination         = [];
                        foreach($stock['variants'] as $indexstockvariant => $variant){
                            $new_combination[]   = $variant->variant_id;
                        }

                        if(!in_array($new_combination,$variant_combinations)){
                            $variant_combinations[] = $new_combination;
                        }
                    }
                }
            }

            $sold_out_variants = [];
            if($variant_combinations){
                foreach($data_all_variants as $indexvariant => $variant){
                    $variant_counter = [];
                    foreach($variant_combinations as $index => $combination){
                        if(in_array($variant,$combination)){
                            $variant_counter[] = $variant;
                        }
                    }

                    if(!sizeof($variant_counter)){
                        $sold_out_variants[] = $variant;
                    }
                }
            }

            unset($data['variants']);
            unset($data['available_stocks']);

            $data['variants']                           = $variants;
            $data['available_variant_combinations']     = $variant_combinations;
            $data['sold_out_variants']                  = $sold_out_variants;


            $result = [
                "data"      => $data,
            ];
            
            return response()->json($result,200);
        }else{
            $result = [
                "response"  => "No product found!"
            ];
            return response()->json($result,404);
        }
    }
}
