<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cart extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        "user_id",
        'shipping_address_id',
        "payment_status",
        "payment_data",
        "tax_value",
        "status"
    ];

    public function owner(){
        return $this->belongsTo(User::class,"user_id","id");
    }

    public function items(){
        return $this->hasMany(CartItem::class);
    }

    public function shipping(){
        return $this->belongsTo(ShippingAddress::class,"shipping_address_id","id");
    }
}