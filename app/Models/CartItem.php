<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CartItem extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        "cart_id",
        'stock_id',
        "quantity",
        "price"
    ];

    public function detail(){
        return $this->belongsTo(Stock::class,"stock_id","id");
    }
}