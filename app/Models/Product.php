<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        "name",
        "slug",
        "description",
        "sku",
        "category_id",
        "user_id"
    ];

    public function variants(){
        return $this->hasMany(Variant::class);
    }

    public function available_stocks(){
        return $this->hasMany(Stock::class)->where("quantity",">",0);
    }
}