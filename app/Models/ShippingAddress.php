<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShippingAddress extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "addresses";

    protected $fillable = [
        "user_id",
        "first_name",
        "last_name",
        "address",
        "landmark",
        "city",
        "state",
        "zip_code",
        "country"
    ];
}