<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stock extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        "product_id",
        "warehouse_id",
        "quantity",
        "price",
        "cost",
        "discount"
    ];

    public function variants(){
        return $this->hasMany(StockVariant::class);
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }
}