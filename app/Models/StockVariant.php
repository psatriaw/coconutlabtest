<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class StockVariant extends Model
{
    use HasFactory;
    use softDeletes;

    protected $table = "stock_variants";
    protected $fillable = ["variant_id","stock_id"];

    public function variant(){
        return $this->belongsTo(Variant::class);
    }
}
