<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Models\{User, ShippingAddress};

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();
            $table->foreignIdFor(User::class)->constrain();
            $table->foreignIdFor(ShippingAddress::class)->nullable();
            $table->string("payment_status")->nullable();
            $table->string("payment_data")->nullable();
            $table->double("tax_value")->nullable();
            $table->string("status",16)->default("cart");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('carts');
    }
};
