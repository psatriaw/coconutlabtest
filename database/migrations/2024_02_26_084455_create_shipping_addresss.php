<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Models\{User};

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();
            $table->foreignIdFor(User::class)->constrain();
            $table->string("country");
            $table->string("first_name",32);
            $table->string("last_name",32)->nullable();
            $table->text("address")->nullable();
            $table->string("landmark",128)->nullable();
            $table->string("city",64)->nullable();
            $table->string("state",64);
            $table->string("zip_code",8);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('addresses');
    }
};
