<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\{ShippingAddress, User};

class CreateDummyAddress extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $user = User::find(1)->first();

        ShippingAddress::create([
            "user_id"       => $user->id,
            "first_name"    => $user->name,   
            "last_name"     => $user->name,
            "address"       => "Cendrawasih 19",
            "landmark"      => "Depan TK",
            "city"          => "Kebumen",
            "state"         => "Central Java",
            "zip_code"      => 54313,
            "country"       => "ID"
        ]);
    }
}
