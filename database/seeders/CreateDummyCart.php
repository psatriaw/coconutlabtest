<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\{User, Product, Variant, Warehouse, Stock, Cart, CartItem};
use Illuminate\Support\Facades\DB;

class CreateDummyCart extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                "type"    => "cart",
                "items"   => [
                    [
                        "name"  => "T-Shirt",
                        "variants"  => [
                            Variant::where("slug","color")->where("name","Green")->first()->id,     //"color" => "Green",
                            Variant::where("slug","size")->where("name","L")->first()->id           //"size"  => "L",
                        ],
                        "quantity"  => 1,
                        "warehouse" => Warehouse::where("name","Surabaya Warehouse")->first()->id
                    ],
                    [
                        "name"  => "T-Shirt",
                        "variants"  => [
                            Variant::where("slug","color")->where("name","Blue")->first()->id,      //"color" => "Blue",
                            Variant::where("slug","size")->where("name","S")->first()->id           //"size"  => "S",
                        ],
                        "quantity"  => 2,
                        "warehouse" => Warehouse::where("name","Jakarta Warehouse")->first()->id
                    ],
                    [
                        "name"  => "Keripik",
                        "variants"  => [
                            Variant::where("slug","spicy_level")->where("name","2")->first()->id,       //"spicy_level" => "2",
                            Variant::where("slug","size")->where("name","100gr")->first()->id           //"size"  => "100gr",
                        ],
                        "quantity"  => 2,
                        "warehouse" => Warehouse::where("name","Surabaya Warehouse")->first()->id
                    ],
                    [
                        "name"  => "Keripik",
                        "variants"  => [
                            Variant::where("slug","spicy_level")->where("name","4")->first()->id,       //"spicy_level" => "4",
                            Variant::where("slug","size")->where("name","200gr")->first()->id           //"size"  => "200gr",
                        ],
                        "quantity"  => 1,
                        "warehouse" => Warehouse::where("name","Surabaya Warehouse")->first()->id
                    ],
                    [
                        "name"  => "Mug",
                        "variants"  => [],
                        "quantity"  => 2,
                        "warehouse" => Warehouse::where("name","Jakarta Warehouse")->first()->id
                    ]
                ]
            ]
        ];


        $user = User::find(1)->first();

        foreach($data as $index => $item){
            $cart = Cart::create([
                "user_id"               => $user->id,
                'shipping_address_id'   => null,
                "payment_status"        => "pending",
                "payment_data"          => null,     
                "tax_value"             => null
            ]);

            foreach($item['items'] as $indexitem => $detail){
                $product = Product::where("name",$detail['name'])->first();

                if($detail['variants']){
                    $stock   = Stock::whereHas("variants",function($query) use ($detail){
                        $query->select(DB::raw("count(*) total"),"stock_id");
                        $query->whereIn("variant_id",$detail['variants']);
                        $query->having("total",2);
                    })
                    ->where("warehouse_id",$detail['warehouse'])
                    ->with('variants.variant')
                    ->first();
                }else{
                    $stock = Stock::whereHas("product",function($query) use ($detail){
                        $query->where("name",$detail['name']);
                    })
                    ->where("warehouse_id",$detail['warehouse'])
                    ->with('variants.variant')
                    ->first();
                }

                CartItem::create([
                    "cart_id"       => $cart->id,
                    'stock_id'      => $stock->id,
                    "quantity"      => $detail['quantity'],
                    "price"         => $stock->price + $this->sumAdditionalPrice($stock->variants)
                ]);

                print_r([
                    "cart_id"       => $cart->id,
                    'stock_id'      => $stock->id,
                    "quantity"      => $detail['quantity'],
                    "price"         => $stock->price + $this->sumAdditionalPrice($stock->variants)
                ]);
            }
        }
    }

    protected function sumAdditionalPrice($variants){
        $total = 0;
        if($variants){
            foreach($variants as $index => $item){
                $total = $total + $item['variant']['additional_price'];
            }
        }
        return $total;
    }
}
