<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\{StockVariant,Product,Variant, Stock, Warehouse, User};

class CreateProductSample extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data   = [
            [
                "name"      => "T-Shirt",
                "sku"       => "tshirt",
                "variants"  => [
                    "color"    => [
                        [
                            "name"  => "Red",
                            "additional_price"  => 0,
                            "additional_cost"   => 0,
                        ],
                        [
                            "name"  => "Green",
                            "additional_price"  => 0,
                            "additional_cost"   => 0,
                        ],
                        [
                            "name"  => "Blue",
                            "additional_price"  => 0,
                            "additional_cost"   => 0,
                        ]
                    ],
                    "size"     => [
                        [
                            "name"  => "S",
                            "additional_price"  => 0,
                            "additional_cost"   => 0,
                        ],
                        [
                            "name"  => "M",
                            "additional_price"  => 0,
                            "additional_cost"   => 0,
                        ],
                        [
                            "name"  => "L",
                            "additional_price"  => 0,
                            "additional_cost"   => 0,
                        ]
                    ]
                ],
                "stocks"    => [
                    [
                        "variants"  => [
                            "size"      => "S",
                            "color"     => "Red",
                        ],
                        "quantity"  => 1000,
                        "price"     => 10,
                        "warehouses"   => ["Jakarta Warehouse","Surabaya Warehouse"]
                    ],
                    [
                        "variants"  => [
                            "size"      => "M",
                            "color"     => "Red",
                        ],
                        "quantity"  => 0,
                        "price"     => 10,
                        "warehouses"   => ["Jakarta Warehouse","Surabaya Warehouse"]
                    ],
                    [
                        "variants"  => [
                            "size"      => "L",
                            "color"     => "Red",
                        ],
                        "quantity"  => 1000,
                        "price"     => 10,
                        "warehouses"   => ["Jakarta Warehouse","Surabaya Warehouse"]
                    ],
                    [
                        "variants"  => [
                            "size"      => "S",
                            "color"     => "Green",
                        ],
                        "quantity"  => 1000,
                        "price"     => 10,
                        "warehouses"   => ["Jakarta Warehouse","Surabaya Warehouse"]
                    ],
                    [
                        "variants"  => [
                            "size"      => "M",
                            "color"     => "Green",
                        ],
                        "quantity"  => 1000,
                        "price"     => 10,
                        "warehouses"   => ["Jakarta Warehouse","Surabaya Warehouse"]
                    ],
                    [
                        "variants"  => [
                            "size"      => "L",
                            "color"     => "Green",
                        ],
                        "quantity"  => 1000,
                        "price"     => 15,
                        "warehouses"   => ["Jakarta Warehouse","Surabaya Warehouse"]
                    ],
                    [
                        "variants"  => [
                            "size"      => "S",
                            "color"     => "Blue",
                        ],
                        "quantity"  => 1000,
                        "price"     => 10,
                        "warehouses"   => ["Jakarta Warehouse","Surabaya Warehouse"]
                    ],
                    [
                        "variants"  => [
                            "size"      => "M",
                            "color"     => "Blue",
                        ],
                        "quantity"  => 1000,
                        "price"     => 10,
                        "warehouses"   => ["Jakarta Warehouse","Surabaya Warehouse"]
                    ],
                    [
                        "variants"  => [
                            "size"      => "L",
                            "color"     => "Blue",
                        ],
                        "quantity"  => 1000,
                        "price"     => 10,
                        "warehouses"   => ["Jakarta Warehouse","Surabaya Warehouse"]
                    ],
                ]
            ],
            [
                "name"      => "Keripik",
                "sku"       => "keripik",
                "variants"  => [
                    "spicy_level"    => [
                        [
                            "name"  => "1",
                            "additional_price"  => 1,
                            "additional_cost"   => 0,
                        ],
                        [
                            "name"  => "2",
                            "additional_price"  => 2,
                            "additional_cost"   => 0,
                        ],
                        [
                            "name"  => "3",
                            "additional_price"  => 3,
                            "additional_cost"   => 0,
                        ],
                        [
                            "name"  => "4",
                            "additional_price"  => 4,
                            "additional_cost"   => 0,
                        ],
                        [
                            "name"  => "5",
                            "additional_price"  => 5,
                            "additional_cost"   => 0,
                        ]
                    ],
                    "size"           => [
                        [
                            "name"  => "100gr",
                            "additional_price"  => 0,
                            "additional_cost"   => 0,
                        ],
                        [
                            "name"  => "200gr",
                            "additional_price"  => 0,
                            "additional_cost"   => 0,
                        ],
                        [
                            "name"  => "300gr",
                            "additional_price"  => 0,
                            "additional_cost"   => 0,
                        ]
                    ]
                ],
                "stocks"    => [
                    [
                        "variants"  => [
                            "spicy_level"   => "1",
                            "size"          => "100gr",
                        ],
                        "quantity"  => 1000,
                        "price"     => 3,
                        "warehouses"   => ["Surabaya Warehouse"]
                    ],
                    [
                        "variants"  => [
                            "spicy_level"   => "2",
                            "size"          => "100gr",
                        ],
                        "quantity"  => 1000,
                        "price"     => 3,
                        "warehouses"   => ["Surabaya Warehouse"]
                    ],
                    [
                        "variants"  => [
                            "spicy_level"   => "3",
                            "size"          => "100gr",
                        ],
                        "quantity"  => 1000,
                        "price"     => 3,
                        "warehouses"   => ["Surabaya Warehouse"]
                    ],
                    [
                        "variants"  => [
                            "spicy_level"   => "4",
                            "size"          => "100gr",
                        ],
                        "quantity"  => 1000,
                        "price"     => 3,
                        "warehouses"   => ["Surabaya Warehouse"]
                    ],
                    [
                        "variants"  => [
                            "spicy_level"   => "5",
                            "size"          => "100gr",
                        ],
                        "quantity"  => 1000,
                        "price"     => 3,
                        "warehouses"   => ["Surabaya Warehouse"]
                    ],
                    [
                        "variants"  => [
                            "spicy_level"   => "1",
                            "size"          => "200gr",
                        ],
                        "quantity"  => 1000,
                        "price"     => 5,
                        "warehouses"   => ["Surabaya Warehouse"]
                    ],
                    [
                        "variants"  => [
                            "spicy_level"   => "2",
                            "size"          => "200gr",
                        ],
                        "quantity"  => 1000,
                        "price"     => 5,
                        "warehouses"   => ["Surabaya Warehouse"]
                    ],
                    [
                        "variants"  => [
                            "spicy_level"   => "3",
                            "size"          => "200gr",
                        ],
                        "quantity"  => 1000,
                        "price"     => 5,
                        "warehouses"   => ["Surabaya Warehouse"]
                    ],
                    [
                        "variants"  => [
                            "spicy_level"   => "4",
                            "size"          => "200gr",
                        ],
                        "quantity"  => 1000,
                        "price"     => 5,
                        "warehouses"   => ["Surabaya Warehouse"]
                    ],
                    [
                        "variants"  => [
                            "spicy_level"   => "5",
                            "size"          => "200gr",
                        ],
                        "quantity"  => 1000,
                        "price"     => 5,
                        "warehouses"   => ["Surabaya Warehouse"]
                    ],
                    [
                        "variants"  => [
                            "spicy_level"   => "1",
                            "size"          => "300gr",
                        ],
                        "quantity"  => 1000,
                        "price"     => 7,
                        "warehouses"   => ["Surabaya Warehouse"]
                    ],
                    [
                        "variants"  => [
                            "spicy_level"   => "2",
                            "size"          => "300gr",
                        ],
                        "quantity"  => 1000,
                        "price"     => 7,
                        "warehouses"   => ["Surabaya Warehouse"]
                    ],
                    [
                        "variants"  => [
                            "spicy_level"   => "3",
                            "size"          => "300gr",
                        ],
                        "quantity"  => 1000,
                        "price"     => 7,
                        "warehouses"   => ["Surabaya Warehouse"]
                    ],
                    [
                        "variants"  => [
                            "spicy_level"   => "4",
                            "size"          => "300gr",
                        ],
                        "quantity"  => 1000,
                        "price"     => 7,
                        "warehouses"   => ["Surabaya Warehouse"]
                    ],
                    [
                        "variants"  => [
                            "spicy_level"   => "5",
                            "size"          => "300gr",
                        ],
                        "quantity"  => 1000,
                        "price"     => 7,
                        "warehouses"   => ["Surabaya Warehouse"]
                    ],
                ]
            ],
            [
                "name"      => "Mug",
                "sku"       => "mug",
                "variants"  => [

                ],
                "stocks"    => [
                    [
                        "variants"  => [],
                        "quantity"  => 1000,
                        "price"     => 10,
                        "warehouses"   => ["Jakarta Warehouse"]
                    ],
                ]
            ]
        ];

        $user = User::find(1);

        foreach($data as $index=>$item){
            $product = Product::create([
                "name"  => $item['name'],
                "sku"   => $item['sku'],
                "user_id"   => $user->id
            ]);

            if($item['variants']){
                foreach($item['variants'] as $indexvariant => $variant){
                    if($variant){
                        foreach($variant as $indexv=>$itemvariant){
                            Variant::create([
                                "product_id"    => $product->id,
                                "name"  => $itemvariant['name'],
                                "slug"  => $indexvariant,
                                "additional_price"  => $itemvariant['additional_price'],
                                "additional_cost"   => $itemvariant['additional_cost']
                            ]);
                        }
                    }
                }
            }
      
            if($item['stocks']){
                foreach($item['stocks'] as $indexstock => $itemstock){
                    if($itemstock['warehouses']){
                        foreach($itemstock['warehouses'] as $indexwarehouse => $warehouse){
                            $stock = Stock::create([
                                "product_id"    => $product->id,
                                "warehouse_id"  => Warehouse::where("name",$warehouse)->first()->id,
                                "quantity"      => $itemstock['quantity'],
                                "price"         => $itemstock['price'],
                                "cost"          => 0,
                                "discount"      => 0
                            ]);

                            if($itemstock['variants']){
                                foreach($itemstock['variants'] as $indexstockvariant => $var){
                                    StockVariant::create([
                                        "variant_id"    => Variant::where("product_id",$product->id)->where("slug",$indexstockvariant)->where("name",$var)->first()->id,
                                        "stock_id"      => $stock->id
                                    ]);
                                }
                            }
                        }
                    }
                }
            }
        }

    }
}
