<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\Models\User;

class CreateUsers extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::create([
            "email"     => "psatriaw@gmail.com",
            "password"  => Hash::make("Admin2024!"),
            "name"      => "Pandu Wiguna",
        ]);

        User::create([
            "email"     => "mahalestine@gmail.com",
            "password"  => Hash::make("User2024!"),
            "name"      => "Iqlima Maharani",
        ]);
    }
}
