<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\{Warehouse};

class CreateWarehouses extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Warehouse::create([
            "name"      => "Jakarta Warehouse",
            "address"   => "Jakarta",
            "phone"     => null
        ]);

        Warehouse::create([
            "name"      => "Surabaya Warehouse",
            "address"   => "Surabaya",
            "phone"     => null
        ]);
    }
}
