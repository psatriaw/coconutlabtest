<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            CreateUsers::class,
            CreateWarehouses::class,
            CreateDummyAddress::class,
            CreateProductSample::class,
            CreateDummyCart::class,
            CreateDummyOrder::class,
        ]);
    }
}
