<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\{Stock};

class MakeRedTshirtSold extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Stock::whereIn("id",[1,2,3,4,5,6])->delete();
    }
}
